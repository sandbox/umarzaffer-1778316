CONTENTS OF THIS FILE
----------------------------------

 * Introduction
 * Requirements
 * Installation
 * Note

INTRODUCTION
----------------------

Current Maintainers:

 * Umar Zaffer <http://drupal.org/user/445970>

Nice help module shows form field help text as poshy tip (Poshy Tip Jquery
Plugin). http://vadikom.com/demos/poshytip/.

This module comes with seven different ready to use CSS styled tooltips.
Various settings including tooltip style along with various other
parameters can be configured at /admin/config/user-interface/nice-help/

REQUIREMENTS
----------------------

Nice Help has one dependency.

Contributed modules
 * Libraries API

INSTALLATION
--------------------

To install Nice Help:

 * Copy the Libraries API module to sites/all/modules or
   sites/sitename/modules as you require.
 * Download the latest version of the Poshy Tip jQuery plugin
   (http://vadikom.com/tools/poshy-tip-jquery-plugin-for-stylish-tooltips/)
   and extract it to sites/all/libraries or sites/sitename/libraries as you
   require. The extracted folder must be named poshytip.
 * Enable Nice Help and all of the modules that it requires.
 * Configure Nice Help /admin/config/user-interface/nice-help


NOTE
--------

Nice help does not support tags within field help text.
Help text would not be rendered as tooltip (but rather normally)
if it contains any sort of markup.
